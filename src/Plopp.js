import React, {useEffect, useState} from 'react';
import axios from 'axios';
import DetailsList from "@skatteetaten/frontend-components/DetailsList";
import Grid from "@skatteetaten/frontend-components/Grid";
import Button from "@skatteetaten/frontend-components/Button";
import Spinner from "@skatteetaten/frontend-components/Spinner";
import MessageBar from "@skatteetaten/frontend-components/MessageBar";

function Plopp() {
    const [rader, setRader] = useState([]);
    const [laster, setLaster] = useState(false);
    const [feilmelding, setFeilmelding] = useState(null);

    const kolonner = [
        {
            key: 'kolonneNavn',
            name: 'Navn',
            fieldName: 'name',
            minWidth: 50,
            maxWidth: 200,
            isResizable: true
        },
        {
            key: 'kolonneMobil',
            name: 'Mobil',
            fieldName: 'mobile',
            minWidth: 50,
            maxWidth: 150,
            isResizable: true,
        },
        {
            key: 'kolonneEpost',
            name: 'E-post',
            fieldName: 'email',
            minWidth: 50,
            maxWidth: 150,
            isResizable: true
        },
        {
            key: 'kolonneActions',
            name: 'Handlinger',
            fieldName: 'actions',
            minWidth: 100,
            maxWidth: 100,
            isResizable: false,
            onRender: (item) => {
                return <Button onClick={(e) => {
                    console.log('Gjør noe gøy her med id ' + item.id);
                }}>Vis</Button>;
            }
        }
    ];

    useEffect(() => {
        setLaster(true);
        setFeilmelding(null);

        axios.get('/api/dataliste')
            .then((respons) => {
                setLaster(false);
                setRader(respons.data);
            }, (error) => {
                setLaster(false);
                setFeilmelding(error.message);
                console.log(JSON.stringify(error));
            })
    }, []);

    return (
        <Grid>
            <Grid.Row>
                <Grid.Col lgPush={2} lg={8}>
                    {feilmelding && <MessageBar>{feilmelding}</MessageBar>}

                    <DetailsList
                        disableSelectionZone={laster}
                        columns={kolonner}
                        items={rader}
                        constrainMode={DetailsList.ConstrainMode.horizontalConstrained}
                    />

                    {laster && <Spinner />}
                </Grid.Col>
            </Grid.Row>
        </Grid>
    );
}

export default Plopp;
