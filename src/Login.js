import React, {useState} from 'react';
import axios from 'axios';
import TextField from "@skatteetaten/frontend-components/TextField";
import Button from "@skatteetaten/frontend-components/Button";
import Grid from "@skatteetaten/frontend-components/Grid";
import {Redirect} from "react-router-dom";
import Spinner from "@skatteetaten/frontend-components/Spinner";
import MessageBar from "@skatteetaten/frontend-components/MessageBar";

function Login() {
    const [navn, setNavn] = useState('');
    const [passord, setPassord] = useState('');
    const [loggerInn, setLoggerInn] = useState(false);
    const [erLoggetInn, setErLoggetInn] = useState(false);
    const [feilmelding, setFeilmelding] = useState(null);


    const loggInn = () => {
        setFeilmelding(null); // nullstiller
        setLoggerInn(true);

        axios.post('/api/login', {brukernavn: navn, passord})
            .then((respons) => {
                console.log(respons.data);
                // HER vil man da typisk lagre unna brukerinfoen man får i retur,
                // men på et litt høyere nivå enn bare i denne komponenten
                // Hint: React Context... https://reactjs.org/docs/context.html ;-)

                setErLoggetInn(true);
                setLoggerInn(false);
            }, (error) => {
                if (error.message === 'Request failed with status code 403') {
                    setFeilmelding('Ukjent brukernavn / passord');
                } else {
                    setFeilmelding(error.message)
                    console.log(JSON.stringify(error));
                }

                setLoggerInn(false);
            })
    }

    if (erLoggetInn) {
        return (
            <Redirect to={'/plopp'}/>
        );
    }

    return (
        <div className="App">
            <Grid padding="0px">
                <Grid.Row>
                    <Grid.Col lg={4} lgPush={4}>
                        <TextField
                            id={'navn'}
                            label={'Navn'}
                            value={navn}
                            onChange={(e, value) => setNavn(value)}
                        />
                    </Grid.Col>
                </Grid.Row>

                <Grid.Row>
                    <Grid.Col lg={4} lgPush={4}>
                        <TextField
                            id={'password'}
                            label={'Passord'}
                            value={passord}
                            onChange={(e, value) => setPassord(value)}
                        />
                    </Grid.Col>
                </Grid.Row>

                <Grid.Row>
                    <Grid.Col lg={4} lgPush={4}>
                        <Button buttonStyle="primary" onClick={loggInn} disabled={loggerInn}>
                            {loggerInn ? <Spinner/> : 'Logg inn'}
                        </Button>
                    </Grid.Col>
                </Grid.Row>

                {feilmelding && <Grid.Row>
                    <Grid.Col lg={4} lgPush={4}>
                        <MessageBar>{feilmelding}</MessageBar>
                    </Grid.Col>
                </Grid.Row>
                }
            </Grid>
        </div>
    );
}

export default Login;
