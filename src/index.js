import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Login from './Login';
import * as serviceWorker from './serviceWorker';
import SkeBasis from "@skatteetaten/frontend-components/SkeBasis";
import { Switch, BrowserRouter, Route, Redirect } from "react-router-dom";
import Plopp from "./Plopp";

ReactDOM.render(
  <SkeBasis>
      <BrowserRouter>
          <h1>Knowit Skatteetaten Designsystem DEMO</h1>

          <Switch>
              <Route exact path={'/login'} component={Login} />
              <Route path={'/plopp'} component={Plopp} />
              <Redirect from={'/'} to={'/login'} />
          </Switch>
      </BrowserRouter>
  </SkeBasis>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
