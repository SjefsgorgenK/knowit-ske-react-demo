const express = require('express');
const bodyParser = require('body-parser');

const app = express()

var jsonParser = bodyParser.json();

app.post('/api/login', jsonParser, (req, res) => {
    console.log(req.body);
    const { brukernavn, passord } = req.body;

    if (brukernavn === 'georg' && passord === 'hemmelig') {
        res.json({
            loggetInn: true,
            brukernavn: 'Julenissen'
        });
    } else {
        res.status(403).json({});
    }
});

app.get('/api/dataliste', (req, res) => {
    res.json([
        {
            id: 1,
            name: 'Japp Stam',
            mobile: '99889988',
            email: 'me@world.com'
        },
        {
            id: 2,
            name: 'Georg er kul',
            mobile: '77887788',
            email: 'minime@mini.me'
        },
        {
            id: 3,
            name: 'Knowit',
            mobile: '00000000',
            email: 'post@knowit.no'
        }
    ]);
});

const nodeJsPort = 9090;
app.listen(nodeJsPort, () => {
    console.log('Backend lytter på port ' + nodeJsPort);
});
